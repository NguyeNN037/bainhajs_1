/* Bai 1
    - Dau vao:
        Số ngày làm
    - Dau ra :
        Tiền lương dựa theo số ngày làm
    - Buoc xu ly:
        1 ngày làm tương đương 100,000
        tienLuong = ngayLam * 100,000
*/
var perdayWork = 100000;
var dayWork = 30;
var salary =  dayWork * perdayWork;
console.log("Tiền lương nhận được là: ", salary);

/*Bai 2
    - Dau vao:
        5 giá trị mà người dùng nhập vào
    - Dau ra:
        Giá trị trung bình của 5 số đó
    - Bước xử li:
        Tạo 5 biến number1 number2 number3 number4 number5
        averageValue = (number1 + number2 + number3 + number4 + number5) / 5;
*/

var number1 = 13;
var number2 = 14;
var number3 = 15;
var number4 = 16;
var number5 = 17;
var averageValue = (number1 + number2 + number3 + number4 + number5) / 5;
console.log("Giá trị trung bình: ", averageValue);

/* Bai 3
    - Dau vao:
        Số tiền USD 
    - Dau ra :
        Tiền việt nam tương xướng với số USD
    - Buoc xu ly:
        1 USD =  23.500 VND
        moneyTran = USDcoin * 23.500;
*/

var USDtoVN = 23.500;
var USDcoin = 13;
var moneyTran =  USDtoVN * USDcoin;
console.log("Tiền sau khi đổi: ", moneyTran);

/* Bai 4
    - Dau vao:
        Chiều dài, chiều rộng
    - Dau ra :
        Diện tích và chu vi hình chữ nhật
    - Buoc xu ly:
        S = lengthRec * widthRec;
        CV = (lengthRec + widthRec)*2;
*/

var lengthRec = 10;
var widthRec  = 15;
var sRec =  (lengthRec * widthRec);
var CVRec = (lengthRec + widthRec) * 2;
console.log("Diện tích: ", sRec);
console.log("Chu vi: ", CVRec);

/* Bai 5
    - Dau vao:
        Số có 2 chữ số vd: 38;
    - Đầu ra:
        Tổng của 2 chữ số đó; a = 3+8 = 11;
    - Bước xử lý:
         tạo 2 biến để lưu trữ dữ liệu
         Tách từng phần tử và gán vào biến đó
         a  = num % 10;
         b = ((number-donVi)/10) % 10;
         sum  = a+b;
*/
var number = 38;
var donVi = number % 10;
var hangChuc = ((number-donVi)/10) % 10;
var sum = donVi + hangChuc;
console.log("Tong là: ", sum);
